<?php

return array(
    //============================== New strings to translate ==============================//
    // Defined in file C:\\wamp\\www\\devagnosevents\\resources\\views\\ManageEvent\\Widgets.blade.php
    'embed_preview'     => 'プレビューを埋め込む',
    //==================================== Translations ====================================//
    'event_widgets'     => 'イベントウィジェット',
    'html_embed_code'   => 'HTML埋め込みコード',
    'instructions'      => '説明書',
    'instructions_text' => 'ウィジェットを表示したい場所にWebサイトのHTMLをコピーして貼り付けるだけです。',
);