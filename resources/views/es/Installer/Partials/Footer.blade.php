<div class="well">
    <p>
        La instalación puede tardar varios minutos en completarse. Una vez que hagas clic en '<b>Instalar DevagnosEvents</b>'
        los ajustes de configuración se escribirán en este archivo: <b>{{base_path('.env')}}</b>. Puedes cambiar
        manualmente estas configuraciones en el futuro editando este archivo.
    </p>
    <p>
        Si la instalación falla, asegúrese de comprobar el archivo de registro en <b>{{storage_path('logs')}}</b>. Si no
        hay errores en los archivos de registro compruebe también <b>otros archivos de registro en su servidor</b>.
    </p>
    <p>
        Si usted está usando hosting compartido, por favor pregunte a su host si soporta los requisitos de DevagnosEvents
        antes de solicitar soporte.
    </p>
    <p>
        Si todavía necesita ayuda, puede <a href="https://github.com/DevagnosEvents/DevagnosEvents/issues">abrir una
            incidencia</a>. Por favor, incluya tantos detalles como sea posible, incluyendo cualquier error en el
        archivo de registro.
        También puedes consultar la <a href="https://www.devagnosevents.com/troubleshooting.html">guía de solución de
            problemas</a>
    </p>
    <p>
        Por favor, también <a style="text-decoration: underline;" target="_blank"
                              href="https://www.devagnosevents.com/license.html">lea la licencia</a> antes de instalar
        DevagnosEvents.
    </p>
</div>
