<div class="well">
    <p>
        Installation may make take several minutes to complete. Once you click '<b>Install DevagnosEvents</b>' the config settings will be written to this file: <b>{{base_path('.env')}}</b>. You can manually change these settings in the future by editing this file.
    </p>
    <p>
        If the install fails be sure to check the log file in <b>{{storage_path('logs')}}</b>. If there are no errors in the log files also <b>check other log files on your server</b>.
    </p>
    <p>
        If you are using shared hosting please ask your host if they support the DevagnosEvents requirements before requesting support.
    </p>
    <p>
        If you still need help you can <a href="https://github.com/DevagnosEvents/DevagnosEvents/issues">raise an issue</a>. Please include as much detail as possible, including any errors in the log file.
        Also take a look at the <a href="https://www.devagnosevents.com/troubleshooting.html">troubleshooting guide</a>
    </p>
    <p>
        Please also  <a style="text-decoration: underline;" target="_blank" href="https://www.devagnosevents.com/license.html">read the licence</a> before installing DevagnosEvents.
    </p>
</div>
