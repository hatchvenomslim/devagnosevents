{{--DevagnosEvents is provided free of charge on the condition the below hyperlink is left in place.--}}
{{--See https://www.devagnosevents.com/license.html for more information.--}}
<div class="powered_by_embedded">
    @include('Shared.Partials.PoweredBy')
</div>
<script type="text/javascript" src="/assets/javascript/iframeSizer.contentWindow.min.js"></script>