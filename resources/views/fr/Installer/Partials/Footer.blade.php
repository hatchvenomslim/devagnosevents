<div class="well">
    <p>
        L'installation peut prendre plusieurs minutes à finir. Une fois que vous cliquez sur '<b>Installer DevagnosEvents</b>' les paramètres de configuration seront écrits dans ce fichier : <b>{{base_path('.env')}}</b>. Vous pouvez manuellement changer ces paramètres plus tard en éditant ce fichier.
    </p>
    <p>
        Si l'installation échoue, assurez-vous de regarder le fichier de journalisation dans <b>{{storage_path('logs')}}</b>. S'il n'y a pas d'erreur dans ce fichier, <b>vérifiez aussi les journaux de votre serveur</b>.
    </p>
    <p>
        Si vous utilisez un hébergement partagé, merci de demander à votre service s'ils répondent aux prérequis nécessaires à l'installation d'DevagnosEvents avant de solliciter du support.
    </p>
    <p>
        Si vous avez encore besoin d'aide, vous pouvez <a href="https://github.com/DevagnosEvents/DevagnosEvents/issues">ouvrir un ticket</a>. Merci d'inclure le plus de détails possible, notamment les erreurs présentes dans les fichiers de journalisation.
        Jetez aussi un œil au <a href="http://www.devagnosevents.com/troubleshooting.html">guide de dépannage</a>
    </p>
    <p>
        Merci aussi de <a style="text-decoration: underline;" target="_blank" href="http://www.devagnosevents.com/license.html">lire la licence</a> avant d'installer DevagnosEvents.
    </p>
</div>
