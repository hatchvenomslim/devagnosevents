<p align="center">
  <img src="http://devagnosevents.website/assets/images/logo-dark.png" alt="DevagnosEvents"/>
  <img style='border: 1px solid #444;' src="https://www.devagnosevents.com/images/screenshots/screen1.PNG" alt="DevagnosEvents"/>
</p>

<h1>DevagnosEvents</h1>
<p>
Open-source ticket selling and event management platform
</p>


> Please report bugs here: https://github.com/DevagnosEvents/DevagnosEvents/issues. Detailed bug reports are more likely to be looked at. Simple creating an issue and saying "it doesn't work" is not useful. Providing some steps to reproduce your problem as well as details about your operating system, PHP version etc can help. <br />

> Take a look http://www.devagnosevents.com/troubleshooting.html and follow the http://www.devagnosevents.com/getting_started.html guide to make sure you have configured devagnosevents correctly.  

Documentation Website: http://www.devagnosevents.com<br />
Demo Event Page: http://devagnosevents.website/e/799/devagnosevents-test-event-w-special-guest-devagnosevents<br />
Demo Back-end Demo: http://devagnosevents.website/signup<br />

*DevagnosEvents* is an open-source event ticketing and event management application built using the Laravel PHP framework. DevagnosEvents was created to offer event organisers a simple solution to managing general admission events, without paying extortionate service fees.

### Current Features (v1.X.X)
---
 - Beautiful mobile friendly event pages
 - Easy attendee management - Refunds, Messaging etc.
 - Data export - attendees list to XLS, CSV etc.
 - Generate print friendly attendee list
 - Ability to manage unlimited organisers / events
 - Manage multiple organisers 
 - Real-time event statistics
 - Customizable event pages
 - Multiple currency support
 - Quick and easy checkout process
 - Customizable tickets - with QR codes, organiser logos etc.
 - Fully brandable - Have your own logos on tickets etc.
 - Affiliate tracking
    - track sales volume / number of visits generated etc.
 - Widget support - embed ticket selling widget into existing websites / WordPress blogs
 - Social sharing 
 - Support multiple payment gateways - Stripe, PayPal & Coinbase so far, with more being added
 - Support for offline payments
 - Refund payments - partial refund & full refunds
 - Ability to add service charge to tickets
 - Messaging - eg. Email all attendees with X ticket
 - Public event listings page for organisers
 - Ability to ask custom questions during checkout
 - Browser based QR code scanner for door management

### Contribution
---
Feel free to fork and contribute. If you are unsure about adding a feature create a Github issue to ask for Feedback. Read the [contribution guidelines](http://www.devagnosevents.com/contributions.html)

### Installation
---
To get developing straight away use the [Pre-configured Docker Environment](http://www.devagnosevents.com/getting_started.html#running-devagnosevents-in-docker-for-development)<br />
To do a manual installation use the [Manual Installation Steps](http://www.devagnosevents.com/getting_started.html#manual-installation)

### Troubleshooting
---
If you are having problems please read the [troubleshooting guide](http://www.devagnosevents.com/troubleshooting.html) 

License
---

DevagnosEvents is open-sourced software licensed under the Attribution Assurance License. See [http://www.devagnosevents.com/license.html](http://www.devagnosevents.com/license.html) for further details. We also have white-label licence options available.

Contributors 
---
* Jeremy Quinton ([Github](https://github.com/jeremyquinton))
* Sam Bell ([Github](https://github.com/samdb))
* Sebastian Schmidt ([Github](https://github.com/publicarray))
* Brett B ([Github](https://github.com/bretto36))
* G0dLik3 ([Github](https://github.com/G0dLik3))
* Honoré Hounwanou ([Github](http://github.com/mercuryseries))
* James Campbell ([Github](https://github.com/jncampbell))
* JapSeyz ([Github](https://github.com/JapSeyz))
* Mark Walet ([Github](https://github.com/markwalet))
